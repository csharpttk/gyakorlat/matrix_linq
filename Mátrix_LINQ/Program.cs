﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mátrix_LINQ
{
    class Program
    {
        const int maxn = 5;
        static int[][] mátrix = new int[maxn][];
       
        static public void generálás()
        {
            for (int i = 0; i < maxn; i++)
                mátrix[i] = new int[maxn];
            Random r = new Random(); //véletlenszám generáláshoz          
            for (int i = 0; i < maxn; i++)
            {
                for (int j = 0; j < maxn; j++)
                {
                    mátrix[i][ j] = r.Next(1,20);
                }
                Console.WriteLine(String.Join(" ",mátrix[i])); //új sor
            }
        }
     
        static public List<int> soronkénti_összeg()
        {
            return mátrix.Select(sor=>sor.Sum()).ToList();            
        }
        static public int elemek_összege()
        {
            return mátrix.Sum(sor => sor.Sum());
        }
        static public List<int> soronkénti_max()
        {
            return mátrix.Select(sor => sor.Max()).ToList();
        }
        static int elemek_maximuma()
        {
            return mátrix.Max(sor => sor.Max());
        }
        static void Main(string[] args)
        {
            Console.WriteLine("A generált mátrix: ");
            generálás();
            Console.WriteLine("\nA soronkénti összeg:");
            Console.WriteLine(String.Join(" ", soronkénti_összeg()));
            Console.WriteLine("\nElemek összege:");
            Console.WriteLine(elemek_összege());
            Console.WriteLine("\nSoronkénti maximum:");
            Console.WriteLine(String.Join(" ", soronkénti_max()));
            Console.WriteLine("\nElemek maximuma:");
            Console.WriteLine(elemek_maximuma());
        }
    }

}
